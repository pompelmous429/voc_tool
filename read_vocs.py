
from __future__ import print_function

import os.path

from google.auth.transport.requests import Request
from google.oauth2.credentials import Credentials
from google_auth_oauthlib.flow import InstalledAppFlow
from googleapiclient.discovery import build
from googleapiclient.errors import HttpError


import traceback
import random
import sys
import os
import sqlite3
from datetime import datetime
import pyttsx3

class bcolors:
    BMAG = '\033[95m' ## bright magenta
    BBL = '\033[94m' ## bright blue
    BCY = '\033[96m' ## bright cyan
    BGR = '\033[92m' ## bright green
    BYEL = '\033[93m' ## bright yellow
    BRED = '\033[91m' ## bright red
    ENDC = '\033[0m' ## end of color
    BOLD = '\033[1m'
    UNDERLINE = '\033[4m'



# If modifying these scopes, delete the file token.json.
SCOPES = ['https://www.googleapis.com/auth/spreadsheets']

# The ID and range of a sample spreadsheet.
VVVV_EN_SHEET_ID = '1YvYja-hpDfZoABR3x2jDkEY4L-7ZyFBH-Qw94zTexEM'
VVVV_CH_SHEET_ID = '1hFIICFSdMO4cEPAJIKdj4e1R7QvHKk0sxkt_NkOde78'
EN_RANGE = 'sheet1!A:Z'
CH_RANGE = 'sheet1!A:Z'

lwords = {}
words = {}
"""
    words = {
        'A': {
            'austere': {
                'pron': '/ɑːˈstɪr/'
                'meaningSet': [
                    {
                        'meaning': '(a.) practicing self-denial (簡樸的；艱苦的；樸素的)',
                        'sentence': '1. His lifestyle of revelry(狂歡，縱酒，作樂) and luxurious excess could hardly be called austere.'
                    },
                    {
                        'meaning': '(a.) unadorned(未經修飾的，樸素的) in style or appearance (不加修飾的，無裝飾的)',
                        'sentence': '1. Late Soviet architecture, although remaining largely austere, moved into experimental territory that employed previously unused shapes and structures.'
                    }
                ]
            }
        },
        'B':
    }
"""
groups = {}

en_path = '..\Downloads\\vvvv_en - sheet1.csv'
ch_path = '..\Downloads\\vvvv_ch - sheet1.csv'
alphabet = [x for x in 'abcdefghijklmnopqrstuvwxyz']
alphabet_words = []
total_count = 0
db_con = None
db_cursor = None
creds = None

txtSpeech = pyttsx3.init()

def init_creds():
    global creds
    if os.path.exists('token.json'):
        creds = Credentials.from_authorized_user_file('token.json', SCOPES)
    if not creds or not creds.valid:
        if creds and creds.expired and creds.refresh_token:
            creds.refresh(Request())
        else:
            flow = InstalledAppFlow.from_client_secrets_file('credentials.json', SCOPES)
            creds = flow.run_local_server(port=0)
        with open('token.json', 'w') as token:
            token.write(creds.to_json())

def _init():
    init_creds()
    global alphabet_words
    for i in range(0, len(alphabet)):
        alphabet_words.append([])
    global db_con, db_cursor
    db_con = sqlite3.connect('voc.db')
    db_cursor = db_con.cursor()
    db_cursor.execute('CREATE TABLE if not exists access_log (word TEXT, access_count INT)')
    db_cursor.execute('CREATE TABLE if not exists day_records (date TEXT, vocs TEXT)')

def parse_all_ch_from_sheet():
    service = build('sheets', 'v4', credentials=creds)
    sheet = service.spreadsheets()
    result = sheet.values().get(spreadsheetId=VVVV_CH_SHEET_ID,
        range=CH_RANGE).execute()
    values = result.get('values', [])
    return values

def readall_ch():
    line_cnt = 0
    try:
        line_cnt += 1
        values = parse_all_ch_from_sheet()
        # print(values)

        pre_title = ''
        for line in values:
            if not line:
                continue
            cur_title = line[0]
            if cur_title == '':
                if pre_title == '': # not start yet
                    continue
                cur_title = pre_title # extended from pre_title
            if cur_title not in groups:
                groups[cur_title] = []
            for token in line[1:]:
                if token == '' or token == '\n':
                    continue
                groups[cur_title].append(token.strip())

            pre_title = cur_title
    except:
        print('[%d] %s' % (line_cnt, traceback.format_exc()))

def getIndexV(src, index):
    if isinstance(src, list):
        return src[index] if len(src) > index else ''

def parse_all_en_from_sheet(): # should have already done init_creds()
    service = build('sheets', 'v4', credentials=creds)
    sheet = service.spreadsheets()
    result = sheet.values().get(spreadsheetId=VVVV_EN_SHEET_ID,
        range=EN_RANGE).execute()
    values = result.get('values', [])
    return values

def readall_en():
    try:
        values = parse_all_en_from_sheet()

        if not values:
            print('[readall_en] cannot get values from sheet')
            return
        
        line_cnt = 0
        cur_prefix = ''
        pre_word = ''

        for line in values:
            # print(line)
            line_cnt += 1

            if not line:
                continue
            if '-prefix' in line[0]: # prefix line
                # cur_prefix = line.split(',')[0].split('-')[0].upper()
                cur_prefix = line[0].split('-')[0].upper()
                words[cur_prefix] = {}
                continue
            if cur_prefix == '': # not start yet
                continue
            
            # tokens = line.split(',')
            word = getIndexV(line, 1)
            pron = getIndexV(line, 2)
            meaning = getIndexV(line, 3)
            synonym = getIndexV(line, 4)
            antonym = getIndexV(line, 5)
            sentence = getIndexV(line, 6)

            if synonym and synonym != '':
                meaning = meaning + ' | ' + synonym

            if line[1] != '' and not (word.startswith(cur_prefix.lower())\
                or word.startswith(cur_prefix.upper())):
                print('#### [%s] error -- prefix = [%s], word = [%s]'\
                    % (line, cur_prefix, word))
            if line[1] == '': # following line of pre_word
                # print('not a new word, append to pre_word [%s]' % pre_word)
                # words[cur_prefix][pre_word]['meaning'].append(meaning)
                tmp_meaningSet = {}
                tmp_meaningSet['meaning'] = meaning
                tmp_meaningSet['sentence'] = sentence
                words[cur_prefix][pre_word]['meaningSet'].append(tmp_meaningSet)
            else: # new word
                words[cur_prefix][word] = {}
                words[cur_prefix][word]['meaningSet'] = []
                tmp_meaningSet = {}
                tmp_meaningSet['meaning'] = meaning
                tmp_meaningSet['sentence'] = sentence
                words[cur_prefix][word]['meaningSet'].append(tmp_meaningSet)
                # words[cur_prefix][word]['meaning'] = [meaning]
                words[cur_prefix][word]['pron'] = pron
                # words[cur_prefix][word]['sentence'] = sentence
                prefix_index = alphabet.index(cur_prefix.lower())
                alphabet_words[prefix_index].append(word)
                pre_word = word

                # check access_count (only do once on every new word)
                dbret = query_word(word)
                if dbret:
                    set_lwords(word, dbret[1])
                    words[cur_prefix][word]['access_count'] = dbret[1]
                else:
                    set_lwords(word, 0)
                    words[cur_prefix][word]['access_count'] = 0
    except:
        print('error line=[%s]' % line_cnt)
        print(traceback.format_exc())

def query_word(word):
    db_cursor.execute("SELECT * FROM access_log WHERE word = '%s'"%word)
    ret = db_cursor.fetchall()
    if len(ret) != 0:
        return ret[0]
    return None

def set_lwords(word, count):
    ## initialization, no need to check word origin position
    global lwords
    if count not in lwords:
        lwords[count] = set()
    lwords[count].add(word)

# def update_lwords(word, op):
#     global words, lwords
#     if op == "add":
        

def print_single_word(word, count=None, indent='', prompt=True, title_c=bcolors.BBL):
    word_tokens = words[word[0].upper()][word]
    if count is None:
        count = 1
    
    printing_lines = []
    print(title_c + '%s==== %d: %s\t%s ====' % (indent, count, word,
        word_tokens['pron']) + bcolors.ENDC, end='')
    # printing_lines.append('==== %d: %s\t%s ====' % (count, word,
    #     word_tokens['pron']))
    for meaningSet in word_tokens['meaningSet']:
        meaning = meaningSet['meaning']
        sentence = meaningSet['sentence']
        printing_lines.append('### %s ###' % meaning)
        printing_lines.append('%s'%sentence)
        printing_lines.append('------------------------------------------------------------------')

    if prompt:
        input()
    else:
        print('') # to make it align with the behavior of input()
    # for line in printing_lines:
    for i in range(len(printing_lines)):
        # if i == 0:
        #     print(indent+printing_lines[i], end='')
        print(indent+printing_lines[i])

def print_word_in_groups(word):
    for group_title in groups:
        word_list = groups[group_title]
        for _word in word_list:
            if word in _word:
                print('')
                print(bcolors.BCY + '>>> %s' % group_title + bcolors.ENDC)
                print('%s' % word_list)
                print('------------------------------------------------------------------')

def print_word_in_others_meaning(word, prompt=True):
    count = 1
    for pref in words:
        for _word in words[pref]:
            for _meaningSet in words[pref][_word]['meaningSet']:
                _meaning = _meaningSet['meaning']
                tokens = _meaning.split('|')
                if len(tokens) < 2:
                    continue
                syns = tokens[1].strip().split('/')
                if word in syns:
                    print_single_word(_word, count, '\t', prompt=prompt, title_c=bcolors.BMAG)
                    count += 1

def list_prefix():
    try:
        while(True):
            prefix = input('\nprefix?\n=>> ')
            try:
                if len(prefix) != 1 or not prefix.isalpha():
                    continue
                prefix_words = words[prefix.upper()]
                print('total: %d' % len(prefix_words))
                count = 0
                for word in prefix_words:
                    print_single_word(word, count)
                    print_word_in_groups(word)
                    count += 1
            except KeyboardInterrupt:
                # continue prompting '\nprefix'
                pass
    except KeyboardInterrupt:
        print('End list_prefix')

def list_group():
    try:
        print('\ntotal: %d' % len(groups))
        input('start?\n')
        count = 1
        for group in groups:
            print('\n----------------')
            print('%d. %s' % (count, group))
            print(groups[group])
            print('----------------')
            input('')
            count += 1
    except KeyboardInterrupt:
        print('End list_group')

def search():
    try:
        while(True):
            word = input('search word?\n=>> ')
            if word not in words[word[0].upper()]:
                continue
            print_single_word(word, prompt=False)
            print_word_in_others_meaning(word, prompt=False)
            print_word_in_groups(word)
            # print_word_sentence(word)
            print('\n')
    except KeyboardInterrupt:
        print('End search')

def find_word_by_count(count):
    for prefix_words in alphabet_words:
        if len(prefix_words) >= count:
            return prefix_words[count-1]
        count -= len(prefix_words)
    print('??? cannot find word by count ???')
    return ''

def print_word_sentence(word):
    if word not in words[word[0].upper()]:
        return
    word_tokens = words[word[0].upper()][word]
    sentence = word_tokens['sentence']
    if sentence != ',':
        print(word_tokens['sentence'])

def random_pick():
    '''
        1. start from the least count in lwords.keys
        2. random pick in that count's set in lwords[count]
        3. after all words in current least count's set depleted, move to new least count
    '''
    try:
        count = 0
        while(True):
            counts = list(lwords.keys())
            counts.sort()
            cur_word = random.choice(list(lwords[counts[0]]))
            print_single_word(cur_word, count, '', True)
            print_word_in_others_meaning(cur_word)
            print_word_in_groups(cur_word)
            count += 1
            increment_word_record(cur_word)
            print(bcolors.BYEL + '=> pronounce? (y)\n' + bcolors.ENDC, end='')
            r = input()
            if r == 'y':
                txtSpeech.say(cur_word)
                txtSpeech.runAndWait()
            print('')
    except KeyboardInterrupt:
        print('End random')

def increment_word_record(word):
    '''
        word being accessed:
        1. update access_count in global words, lwords
        2. db operation
            => table 'access_log': increment column 'access_count'
            => table 'day_records': add this word to today date
    '''
    ## ======== update global words, lwords ========
    global words, lwords
    cur_count = words[word[0].upper()][word]['access_count']
    lwords[cur_count].remove(word)
    if len(lwords[cur_count]) == 0:
        lwords.pop(cur_count, None)
    set_lwords(word, cur_count+1)
    words[word[0].upper()][word]['access_count'] += 1

    ## ======== update db record ========
    global db_cursor
    # 1. table 'access_log'
    db_cursor.execute("SELECT EXISTS(SELECT * from access_log WHERE word"\
        " = '%s')"%word)
    existing = db_cursor.fetchall()[0][0]
    if existing:
        db_cursor.execute("UPDATE access_log SET access_count = access_count + 1"\
            " WHERE word = '%s'"% (word))
    else:
        db_cursor.execute("INSERT INTO access_log (word, access_count)"\
            " VALUES ('%s', %d)"%(word, 1))
    # 2. table 'day_records'
    now = datetime.now()
    dt_str = now.strftime("%Y-%m-%d")
    db_cursor.execute("SELECT EXISTS(SELECT * from day_records WHERE date"\
                      " = '%s')"%dt_str)
    existing = db_cursor.fetchall()[0][0]
    if existing: ## check if this word already appended
        db_cursor.execute("SELECT * from day_records WHERE date = '%s'"%dt_str)
        recs = db_cursor.fetchall()[0][1].split(';')
        if word not in recs:
            db_cursor.execute("UPDATE day_records SET vocs = vocs || ';%s'"\
                "WHERE date = '%s'"%(word, dt_str))
    else:
        db_cursor.execute("INSERT INTO day_records (date, vocs) VALUES('%s',"\
                          " '%s')"%(dt_str, word))

    db_con.commit()

def check_today():
    now = datetime.now()
    dt_str = now.strftime("%Y-%m-%d")
    db_cursor.execute("SELECT  * from day_records WHERE date = '%s'"%dt_str)
    ret = db_cursor.fetchall()
    if len(ret) == 0:
        print(bcolors.BMAG + '[%s] no words learned yet!'%dt_str + bcolors.ENDC)
        return
    today_words = ret[0][1].split(';')
    print(bcolors.BRED + '[%s] finished: [%d]'%(dt_str, len(today_words)) + bcolors.BBL + '\n%s' % (str(today_words)) + bcolors.ENDC)

def debug_info(option):
    try:
        print(bcolors.BRED + '-------- debugging --------' + bcolors.ENDC)
        option = option.split(':')
        if len(option) == 1: ## no appending :<word>
            print(words)
            print(lwords)
            return

        # debug lwords, with specific count. Ex: D:lwords:1
        if option[1] == 'lwords':
            if len(option) > 2:
                if option[2] == 'keys':
                    print(list(lwords.keys()))
                    return
                print(lwords[int(option[2])])
                print('count=[%d]'%len(lwords[int(option[2])]))
            else:
                print(lwords)
            return

        # debug single word
        word = option[1]
        if word not in words[word[0].upper()]:
            return
        print(words[word[0].upper()][word])
        # check in lwords?
        db_cursor.execute("SELECT * from access_log WHERE word = '%s'"%word)
        ret = db_cursor.fetchall()
        print(ret)
        if len(ret) != 0:
            print('access_count=[%d]'%ret[0][1])
    except:
        print('[debug_info] %s'%traceback.format_exc())
    finally:
        print(bcolors.BRED + '-------- ending debug --------' + bcolors.ENDC)

def main():
    try:
        _init()
        readall_ch()
        readall_en()
        # print(words)
        # print(lwords)
        # print(groups)
        global total_count
        total_count = 0
        for i in range(0, len(alphabet)):
            total_count += len(words[alphabet[i].upper()])
        # print(total_count)
        if len(sys.argv) > 1:
            func = globals().get(sys.argv[1])
            word = sys.argv[2]
            return func(word)
            
        while(True):
            print('options?\n(1) list_prefix (2) '\
                'list_group (3) search (4) random (5) check today progress')
            option = input('')
            ## debug
            if option.startswith('D'):
                debug_info(option)
            
            if not option.isdigit():
                continue
            option = int(option)
            if option == 1:
                list_prefix()
            elif option == 2:
                list_group()
            elif option == 3:
                search()
            elif option == 4:
                random_pick()
            elif option == 5:
                check_today()
    except KeyboardInterrupt:
        print('Exit')
    except:
        print(traceback.format_exc())

if __name__ == '__main__':
    main()


 