# voc_tool

a tool for conveniently reviewing vocabulary organized in google sheet

## Getting started

1. enable Google Sheet API guided in https://developers.google.com/sheets/api/quickstart/python

2. vocabulary google sheets in the format

EN:https://docs.google.com/spreadsheets/d/1YvYja-hpDfZoABR3x2jDkEY4L-7ZyFBH-Qw94zTexEM/edit?usp=sharing
CH:https://docs.google.com/spreadsheets/d/1hFIICFSdMO4cEPAJIKdj4e1R7QvHKk0sxkt_NkOde78/edit?usp=sharing

3. check the demo video for how to use this tool https://youtu.be/uBNJ4qx-FK8


